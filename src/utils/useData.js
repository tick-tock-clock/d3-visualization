import {csv, json} from 'd3';
import {useCallback, useEffect, useState} from 'react';

const topojson = require('topojson');

const projects = {

    cities: {
        url:
            'https://gist.githubusercontent.com/curran/13d30e855d48cdd6f22acdf0afe27286/raw/0635f14817ec634833bb904a47594cc2f5f9dbf8/worldcities_clean.csv',
        formatReader: (url, row) => csv(url, row),
        handleData: (set, d) => set(d),
        transformations: (d) => {
            d.lat = +d.lat;
            d.lng = +d.lng;
            d.population = +d.population;
            return d;
        },
        slicing: (d) => d,
    },
    world: {
        url:
            'https://cdn.jsdelivr.net/npm/world-atlas@2/countries-50m.json',
        formatReader: (url, _) => json(url),
        handleData: (set, d) => {
            const {countries, land} = d.objects;
            set({
                land: topojson.feature(d, land),
                countries: topojson.feature(d, countries),
                interiors: topojson.mesh(d, countries, (a, b) => a !== b)
            });
        },
        transformations: (d) => d,
        slicing: (d) => d,
    },
    temperature: {
        url:
            'https://gist.githubusercontent.com/curran/90240a6d88bdb1411467b21ea0769029/raw/7d4c3914cc6a29a7f5165f7d5d82b735d97bcfe4/week_temperature_sf.csv',
        formatReader: (url, transformations) => csv(url, transformations),
        handleData: (set, d) => set(d),
        transformations: (d) => {
            d.temperature = +d.temperature;
            d.timestamp = new Date(d.timestamp).getTime();
            return d;
        },
        slicing: (d) => d,
    },
    population: {
        url:
            'https://gist.githubusercontent.com/curran/0ac4077c7fc6390f5dd33bf5c06cb5ff/raw/605c54080c7a93a417a3cea93fd52e7550e76500/UN_Population_2019.csv',
        formatReader: (url, transformations) => csv(url, transformations),
        handleData: (set, d) => set(d),
        transformations: (d) => {
            d.Population = +d['2020'] * 1000;
            return d;
        },
        slicing: (d) => d.slice(0, 10),
    },
    iris: {
        url:
            'https://gist.githubusercontent.com/curran/a08a1080b88344b0c8a7/raw/0e7a9b0a5d22642a06d3d5b9bcbad9890c8ee534/iris.csv',
        formatReader: (url, transformations) => csv(url, transformations),
        handleData: (set, d) => set(d),
        transformations: (d) => {
            d.sepal_length = +d.sepal_length;
            d.sepal_width = +d.sepal_width;
            d.petal_length = +d.petal_length;
            d.petal_width = +d.petal_width;
            return d;
        },
        slicing: (d) => d
    }
}

export const useData = (project) => {
    const [data, setData] = useState(null);

    console.log(data);
    const fetchData = useCallback(() => {
        projects[project].formatReader(
            projects[project].url,
            projects[project].transformations
        ).then((d) => projects[project].handleData(setData, d));
    }, [project, setData])

    useEffect(() => {
        console.log("Fetching Data")
        fetchData()
    }, [fetchData]);

    return data;
};
