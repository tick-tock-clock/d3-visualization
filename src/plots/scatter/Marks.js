export const Marks = ({
                          data,
                          xScale,
                          yScale,
                          xValue,
                          yValue,
                          colorScale,
                          colorValue,
                          tooltipFormat = (v) => v,
                      }) =>
    data.map((d, idx) => (
        <circle
            key={`circle-${idx}`}
            cx={xScale(xValue(d))}
            cy={yScale(yValue(d))}
            fill={colorScale(colorValue(d))}
            r={5}
        >
            <title>{tooltipFormat(xValue(d))}</title>
        </circle>
    ));
