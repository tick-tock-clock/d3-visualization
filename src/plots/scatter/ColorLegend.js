export const ColorLegend = ({
                                x,
                                y,
                                colorScale,
                                legendLabel,
                                onHover,
                                hoveredValue,
                                fadeOpacity,
                                tickSpacing = 20,
                                tickSize = 5,
                                tickLabelOffset = 10,
                                tickCircleOffset = 5
                            }) => (
    <g transform={`translate(${x}, ${y})`}>
        <text
            className="axis-label"
            transform={`translate(${0}, ${0}) rotate(0)`}
        >
            {legendLabel}
        </text>
        {colorScale.domain().map((item, idx) => (
            <g key={idx} className="color-legend-item"
               transform={`translate(15, ${idx * tickSpacing + 30})`}
               onMouseEnter={() => {
                   onHover(item);
               }}
               onMouseLeave={() => {
                   onHover(null);
               }}
               opacity={hoveredValue && hoveredValue !== item ? fadeOpacity : 1}
            >
                <circle cy={-tickCircleOffset} r={tickSize} fill={colorScale(item)}/>
                <text className="tick" x={tickLabelOffset}>{item.toUpperCase()}</text>
            </g>
        ))}
    </g>
)