import {useState} from 'react';
import {extent, scaleLinear, scaleOrdinal} from 'd3';

import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';


import {useData} from '../../utils/useData';
import {AxisBottom} from "./AxisBottom";
import {AxisLeft} from "./AxisLeft";
import {Marks} from "./Marks";
import {ColorLegend} from "./ColorLegend";

const width = window.innerWidth;
const height = window.innerHeight;
const margin = {
    top: height / 30,
    left: width / 10,
    bottom: height / 7.5,
    right: width / 5,
};

const menuHeight = 80;

const innerHeight = height - margin.top - margin.bottom - menuHeight;
const innerWidth = width - margin.left - margin.right;
const options = [
    {value: 'sepal_width', label: "Sepal Width"},
    {value: 'sepal_length', label: "Sepal Length"},
    {value: 'petal_length', label: "Petal Length"},
    {value: 'petal_width', label: "Petal Width"},
]
const fadeOpacity = 0.3;

const filterLabel = ({value, label}, attribute) => (
    options.filter(({value, label}) => value === attribute)[0].label
);

export const ScatterPlot = () => {
        const data = useData("iris");

        const initialXAttribute = 'sepal_length';
        const [xAtrribute, setXAttribute] = useState(initialXAttribute);
        const xValue = (d) => d[xAtrribute];
        const xLabel = filterLabel(options, xAtrribute);

        const initialYAttribute = 'sepal_width';
        const [yAttribute, setYAttribute] = useState(initialYAttribute);
        const yValue = (d) => d[yAttribute];
        const yLabel = filterLabel(options, yAttribute);

        const colorValue = (d) => d.species;

        const [hoveredValue, setHoveredValue] = useState(null);

        if (!data) {
            return <pre>Loading...</pre>;
        }

        const filteredData = data.filter(d => colorValue(d) === hoveredValue);

        const xScale = scaleLinear()
            .domain(extent(data, xValue))
            .range([0, innerWidth])
            .nice();

        const yScale = scaleLinear()
            .domain(extent(data, yValue))
            .range([0, innerHeight])
            .nice();

        const colorScale = scaleOrdinal()
            .domain(data.map(colorValue))
            .range(["#E6842A", "#137B80", "#8E6C8A"])

        return (
            <>
                <div className="menus-container">
                    <span className="dropdown-label">X</span>
                    <Dropdown
                        options={options}
                        onChange={option => setXAttribute(option.value)}
                        value={xAtrribute}/>

                    <span className="dropdown-label">Y</span>
                    <Dropdown
                        options={options}
                        onChange={option => setYAttribute(option.value)}
                        value={yAttribute}/>
                </div>

                <svg width={width} height={height}>
                    <g
                        transform={`translate(${margin.left},${margin.top})`}
                    >
                        <AxisBottom
                            xScale={xScale}
                            innerHeight={innerHeight}
                            innerWidth={innerWidth}
                            axisLabelOffset={60}
                            axisLabel={xLabel}
                            tickOffset={10}
                        />

                        <AxisLeft yScale={yScale}
                                  innerWidth={innerWidth}
                                  innerHeight={innerHeight}
                                  tickOffset={7}
                                  axisLabelOffset={50}
                                  axisLabel={yLabel}/>
                        <ColorLegend
                            x={innerWidth + 30}
                            y={20}
                            colorScale={colorScale}
                            legendLabel={"Species"}
                            onHover={setHoveredValue}
                            hoveredValue={hoveredValue}
                            fadeOpacity={fadeOpacity}
                            tickSpacing={20}
                            tickSize={5}
                            tickLabelOffset={10}
                            tickCircleOffset={4}/>

                        <g opacity={hoveredValue ? fadeOpacity : 1}>

                            <Marks
                                data={data}
                                xScale={xScale}
                                yScale={yScale}
                                xValue={xValue}
                                yValue={yValue}
                                colorScale={colorScale}
                                colorValue={colorValue}
                            />
                        </g>
                        <Marks
                            data={filteredData}
                            xScale={xScale}
                            yScale={yScale}
                            xValue={xValue}
                            yValue={yValue}
                            colorScale={colorScale}
                            colorValue={colorValue}
                        />
                    </g>
                </svg>
            </>
        );
    }
;
