import {line, curveNatural} from 'd3';

export const Marks = ({
                          data,
                          xScale,
                          yScale,
                          xValue,
                          yValue,
                          tooltipFormat = (v) => v,
                      }) => {
    const linePath = line()
        .x(d => xScale(xValue(d)))
        .y(d => yScale(yValue(d)))
        .curve(curveNatural);

    return <g className="line">
        <path d={linePath(data)} />
        {
            // data.map((d, idx) =>
            // <circle
            //     key={`circle-${idx}`}
            //     className="mark"
            //     cx={xScale(xValue(d))}
            //     cy={yScale(yValue(d))}
            //     r={5}
            // >
            //     <title>{tooltipFormat(xValue(d))}</title>
            // </circle>
        // )
        }
    </g>
}

