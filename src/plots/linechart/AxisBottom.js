export const AxisBottom = ({
                               xScale,
                               innerHeight,
                               innerWidth,
                               axisLabel,
                               tickOffset = 3,
                               axisLabelOffset = 0,
                               tickFormat = (v) => v
                           }) => (
    <g>
        <text
            textAnchor="middle"
            className="axis-label"
            y={innerHeight + axisLabelOffset}
            x={innerWidth / 2}
        >
            {axisLabel}
        </text>

        {xScale.ticks().map((tickValue) => (
            <g
                className="tick"
                key={tickValue}
                transform={`translate(${xScale(tickValue)}, 0)`}
            >
                <line y2={innerHeight}/>

                <text
                    style={{textAnchor: 'middle'}}
                    y={innerHeight + tickOffset}
                    dy=".71em"
                >
                    {tickFormat(tickValue)}
                </text>
            </g>
        ))}
    </g>
);
