export const AxisLeft = (
    {
        yScale,
        innerWidth,
        innerHeight,
        axisLabel,
        axisLabelOffset,
        tickOffset = 3
    }) => (
    <g>
        <text
            textAnchor="middle"
            className="axis-label"
            transform={`translate(${-axisLabelOffset}, ${innerHeight / 2}) rotate(-90)`}
        >
            {axisLabel}
        </text>

        {yScale.ticks().map((tickValue, idx) => (
            <g className="tick"
               key={`left-tick-${idx}`}
               transform={`translate(0, ${yScale(tickValue)})`}
            >
                <line x2={innerWidth}/>
                <text
                    x={-tickOffset}
                    style={{textAnchor: 'end'}}
                    dy=".32em"
                >
                    {tickValue}
                </text>
            </g>
        ))}
    </g>
);

