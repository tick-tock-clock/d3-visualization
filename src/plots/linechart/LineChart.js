import {extent, scaleLinear, scaleTime, timeFormat} from 'd3';
import {useData} from '../../utils/useData';
import {AxisBottom} from "./AxisBottom";
import {AxisLeft} from "./AxisLeft";
import {Marks} from "./Marks";

const width = window.innerWidth;
const height = window.innerHeight;
const margin = {
    top: 20,
    left: 90,
    bottom: 65,
    right: 30,
};
const innerHeight = height - margin.top - margin.bottom;
const innerWidth = width - margin.left - margin.right;

export const LineChart = () => {
    const data = useData("temperature");
    if (!data) {
        return <pre>Loading...</pre>;
    }

    const xValue = (d) => d.timestamp;
    const xLabel = "Time";
    const yValue = (d) => d.temperature;
    const yLabel = "Temperature";

    const timeFormatter = timeFormat("%a ");

    const xScale = scaleTime()
        .domain(extent(data, xValue))
        .range([0, innerWidth])
        .nice();

    const yScale = scaleLinear()
        .domain(extent(data, yValue))
        .range([innerHeight, 0])
        .nice() ;

    return (
        <>
            <svg width={width} height={height}>
                <g
                    transform={`translate(${margin.left},${margin.top})`}>
                    <AxisBottom
                        xScale={xScale}
                        innerHeight={innerHeight}
                        innerWidth={innerWidth}
                        axisLabelOffset={50}
                        axisLabel={xLabel}
                        tickOffset={10}
                        tickFormat={(tickValue) => timeFormatter(tickValue)}

                    />
                    <AxisLeft yScale={yScale}
                              innerWidth={innerWidth}
                              innerHeight={innerHeight}
                              tickOffset={7}
                              axisLabelOffset={40}
                              axisLabel={yLabel}/>
                    <Marks
                        data={data}
                        xScale={xScale}
                        yScale={yScale}
                        xValue={xValue}
                        yValue={yValue}

                    />
                </g>
            </svg>
        </>
    )
        ;
};
