export const AxisLeft = ({ yScale,x,y }) =>
  yScale.domain().map((tickValue) => (
    <g className="tick">
      <text
        key={tickValue}
        x={x}
        y={y}
        style={{ textAnchor: 'end' }}
        dy=".32em"
      >
        {tickValue}
      </text>
    </g>
  ));
