import {scaleLinear, max, scaleBand, format} from 'd3';
import {useData} from '../../utils/useData';
import {AxisBottom} from './AxisBottom';
import {AxisLeft} from './AxisLeft';
import {Marks} from './Marks';

const width = 960;
const height = 500;
const margin = {
    top: 20,
    left: 220,
    bottom: 65,
    right: 30,
};
const innerHeight = height - margin.top - margin.bottom;
const innerWidth = width - margin.left - margin.right;
const axisLabelOffset = 50;

const siFormat = format('.2s');
const xAxisTickFormat = (tickValue) =>
    siFormat(tickValue).replace('G', 'B');

export const BarChart = () => {
    const data = useData();

    if (!data) {
        return <pre>Loading...</pre>;
    }

    const yValue = (d) => d.Country;
    const xValue = (d) => d.Population;

    const yScale = scaleBand()
        .domain(data.map(yValue))
        .range([0, innerHeight])
        .paddingInner(0.15);

    const xScale = scaleLinear()
        .domain([0, max(data, xValue)])
        .range([0, innerWidth]);
    console.log(xScale.range()[1] / 2);

    return (
        <svg width={width} height={height}>
            <g
                transform={`translate(${margin.left},${margin.top})`}
            >
                <AxisBottom
                    xScale={xScale}
                    innerHeight={innerHeight}
                    innerWidth={innerWidth}
                    axisLabelOffset={axisLabelOffset}
                    tickFormat={xAxisTickFormat}
                />
                <AxisLeft yScale={yScale} y={(tickValue) => yScale(tickValue) + yScale.bandwidth() / 2} x={-3}/>
                <Marks
                    data={data}
                    xScale={xScale}
                    yScale={yScale}
                    xValue={xValue}
                    yValue={yValue}
                    tooltipFormat={xAxisTickFormat}
                />
            </g>
        </svg>
    );
};
