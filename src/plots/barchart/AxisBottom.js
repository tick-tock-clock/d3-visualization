export const AxisBottom = ({
  xScale,
  innerHeight,
  innerWidth,
  axisLabelOffset = 0,
  tickFormat = (v) => v
}) => (
  <g>
    <text
      textAnchor="middle"
      className="axis-label"
      y={innerHeight + axisLabelOffset}
      x={innerWidth / 2}
    >
      Population
    </text>

    {xScale.ticks().map((tickValue) => (
      <g
        className="tick"
        key={tickValue}
        transform={`translate(${xScale(tickValue)}, 0)`}
      >
        <line y2={innerHeight} />

        <text
          style={{ textAnchor: 'middle' }}
          y={innerHeight + 3}
          dy=".71em"
        >
          {tickFormat(tickValue)}
        </text>
      </g>
    ))}
  </g>
);
