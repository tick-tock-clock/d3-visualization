import {scaleSqrt, max} from 'd3';
import {useData} from '../../utils/useData';
import {Marks} from "./Marks";

const width = window.innerWidth;
const height = window.innerHeight;

const maxRadius = 20;

export const WorldMap = () => {
    const worldData = useData("world");
    const cityData = useData("cities");

    if (!worldData || !cityData) {
        return <pre>Loading...</pre>;
    }

    const scaleValue = d => d.population;

    const scaleRadius = scaleSqrt()
        .domain([0, max(cityData, scaleValue)])
        .range([0, maxRadius])

    return (
        <svg width={width} height={height}>
            <Marks worldData={worldData} cityData={cityData} scaleRadius={scaleRadius} scaleValue={scaleValue}/>
        </svg>
    );
};
