import {geoEqualEarth, geoGraticule, geoPath} from 'd3';

const projection = geoEqualEarth();
const path = geoPath(projection);
const graticules = geoGraticule();
export const Marks = ({worldData: {countries, land, interiors}, cityData, scaleRadius, scaleValue}) => {

    return (
        <g>
            <path className="sphere" d={path({type: 'Sphere'})}/>
            <path className="graticules" d={path(graticules())}/>
            {countries.features.map((feature, idx) => <path className="country" key={idx} d={path(feature)}/>)}
            <path className="interiors" d={path(interiors)}/>
            {cityData.map(d => {
                const [x, y] = projection([d.lng, d.lat]);
                return <circle key={`${d.lng}-${d.lat}`}
                               className="city-circle"
                               cx={x}
                               cy={y}
                               r={scaleRadius(scaleValue(d))}
                               opacity={0.5}
                />
            })}
        </g>);
};

