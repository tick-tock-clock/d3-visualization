import './App.css';
import {WorldMap} from "./plots/worldmap/WorldMap";

const App = () => <WorldMap/>

export default App;
